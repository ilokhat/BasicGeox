package fr.imran.exemple;

import fr.ign.cogit.geoxygene.api.feature.IFeature;
import fr.ign.cogit.geoxygene.api.feature.IPopulation;
import fr.ign.cogit.geoxygene.api.spatial.AbstractGeomFactory;
import fr.ign.cogit.geoxygene.api.spatial.geomroot.IGeometry;
import fr.ign.cogit.geoxygene.spatial.geomengine.AbstractGeometryEngine;
import fr.ign.cogit.geoxygene.spatial.geomengine.GeometryEngine;
import fr.ign.cogit.geoxygene.util.conversion.ShapefileReader;

public class BasicApp {

	public static void main(String[] args) {
		//initialize engine
		GeometryEngine.init();
		AbstractGeomFactory factory = AbstractGeometryEngine.getFactory();

		String path = "/home/mac/hdd/code/workspace/BasicGeox/target/classes/REU_adm_shp/REU_adm0.shp";
		IPopulation<IFeature> polygonShp = ShapefileReader.read(path);
		
		for (IFeature f: polygonShp){
			IGeometry geom = f.getGeom();
			System.out.println("Centroide de la Réunion : "+ factory.createPoint(geom.centroid()));
		}
	}
}
